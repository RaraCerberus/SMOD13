//========= Made by The SMOD13 Team, Some rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "c_weapon__stubs.h"
#include "basehlcombatweapon_shared.h"
#include "c_basehlcombatweapon.h"
#include "hl1_basecombatweapon_shared.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

STUB_WEAPON_CLASS(weapon_cguard, WeaponCGuard, C_BaseHL1CombatWeapon);
STUB_WEAPON_CLASS(weapon_deagle, WeaponDeagle, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_manhack, WeaponManhack, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_ar2x, WeaponAR2X, C_HLMachineGun);
STUB_WEAPON_CLASS(weapon_pistol_akimbo, WeaponPistolAkimbo, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_beretta, WeaponBeretta, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_beretta_akimbo, WeaponBerettaAkimbo, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_m16, WeaponM16, C_HLSelectFireMachineGun);
STUB_WEAPON_CLASS(weapon_sniperrifle, WeaponSniperRifle, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_uzi, WeaponUZI, C_HLSelectFireMachineGun);
STUB_WEAPON_CLASS(weapon_uzi_akimbo, WeaponUZIAkimbo, C_HLSelectFireMachineGun);
STUB_WEAPON_CLASS(weapon_snark, WeaponSnarkHD, C_BaseHL1CombatWeapon);
STUB_WEAPON_CLASS(weapon_mattspipe, MattsPipe, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_blackhole, WeaponHopwire, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_smg2, WeaponSMG2, C_HLSelectFireMachineGun);
STUB_WEAPON_CLASS(weapon_immolator, WeaponImmolator, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_katana, WeaponKatana, C_BaseHLBludgeonWeapon);
STUB_WEAPON_CLASS(weapon_positiongrabber, WeaponPositionGrabber, C_BaseHLCombatWeapon);
STUB_WEAPON_CLASS(weapon_m249, WeaponM249, C_HLSelectFireMachineGun);
STUB_WEAPON_CLASS(weapon_physlauncher, WeaponPhysLauncher, C_BaseHLCombatWeapon);
