// Elite

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"#SMOD_BerettaAkimbo"
	"viewmodel"			"models/weapons/v_beretta_akimbo.mdl"
	"playermodel"		"models/weapons/w_beretta.mdl"
	"anim_prefix"		"pistol"
	"bucket"			"disabled"	//akimbo ver is hidden
	"bucket_position"	"0"

	"clip_size"			"30"
	"primary_ammo"		"Pistol"
	"secondary_ammo"	"None"
	
	"item_flags"		"0"
	
	"BuiltRightHanded"			"0"
	"addfov"					"20"
	"MuzzleFlashAttachement"	"1"
	"UsesCSMuzzleFlash"			"1"
	"CSMuzzleFlashType"			"CS_MUZZLEFLASH"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"reload_npc"	"Weapon_Pistol.NPC_Reload"
		"empty"			"Weapon_Pistol.Empty"
		"single_shot"	"Weapon_Elite.Single"
		"single_shot_npc"	"Weapon_Elite.Single"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"CSWeaponIcons"
				"character"	"s"
		}
		"weapon_s"
		{	
				"font"		"CSWeaponIconsSelected"
				"character"	"s"
		}
		"weapon_small"
		{
				"font"		"CSWeaponIconsSmall"
				"character"	"s"
		}
		"ammo"
		{
				"font"		"WeaponIconsSmall"
				"character"	"p"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}