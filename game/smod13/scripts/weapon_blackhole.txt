WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"				"#SMOD_BlackHole"
	"viewmodel"				"models/weapons/v_blackhole.mdl"
	"playermodel"			"models/weapons/w_blackhole.mdl"
	"anim_prefix"			"Grenade"
	"bucket"				"grenades"
	"bucket_position"		"4"

	"clip_size"				"-1"
	"clip2_size"			"-1"
	"default_clip"			"1"
	"default_clip2"			"-1"

	"primary_ammo"			"BlackHole"
	"secondary_ammo"		"None"

	"item_flags"			"18"	// ITEM_FLAG_NOAUTORELOAD | ITEM_FLAG_EXHAUSTIBLE

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"special1"		"WeaponFrag.Roll"
		"double_shot"	"common/null.wav"
		"single_shot"	"common/null.wav"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/w_icons3"
				"x"			"1"
				"y"			"129"
				"width"		"126"
				"height"	"62"
		}
		"weapon_s"
		{	
				"file"		"sprites/w_icons3"
				"x"			"1"
				"y"			"129"
				"width"		"126"
				"height"	"62"
		}
		"weapon_small"
		{
				"file"		"sprites/w_icons3"
				"x"			"1"
				"y"			"129"
				"width"		"126"
				"height"	"62"
		}
		"ammo"
		{

				"file"		"sprites/w_icons3"
				"x"			"1"
				"y"			"129"
				"width"		"126"
				"height"	"62"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"48"
				"y"			"72"
				"width"		"24"
				"height"	"24"
		}
	}
}