// Physics Gun

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"				"#SMOD_PhysGun"
	"viewmodel"				"models/weapons/v_physics_gun.mdl"
	"playermodel"			"models/weapons/w_Physics.mdl"
	"anim_prefix"			"physgun"
	"bucket"				"tool"
	"bucket_position"		"1"

	"clip_size"				"-1"
	"clip2_size"			"-1"
	"primary_ammo"			"None"
	"secondary_ammo"		"None"

	"item_flags"			"2"

	SoundData
	{
		"single_shot"		"Weapon_Physgun.On"
		"reload"			"Weapon_Physgun.Off"
		"special1"			"Weapon_Physgun.Special1"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"WeaponIcons"
				"character"	"m"
		}
		"weapon_s"
		{	
				"font"		"WeaponIconsSelected"
				"character"	"m"
		}
		"weapon_small"
		{	
				"font"		"WeaponIconsSmall"
				"character"	"m"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"			"48"
			"y"			"72"
			"width"		"24"
			"height"	"24"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"48"
				"y"			"72"
				"width"		"24"
				"height"	"24"
		}
	}
}