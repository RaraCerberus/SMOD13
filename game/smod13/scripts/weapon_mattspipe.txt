WeaponData
{
	"printname"				"#SMOD_Pipe"
	"viewmodel"				"models/weapons/v_pipe.mdl"
	"playermodel"			"models/props_canal/mattpipe.mdl"
	"anim_prefix"			"crowbar"
	"bucket"				"melee"
	"bucket_position"		"3"

	"clip_size"				"-1"
	"primary_ammo"			"None"
	"secondary_ammo"		"None"

	"weight"				"0"
	"item_flags"			"0"

	SoundData
	{
		"single_shot"		"Weapon_Pipe.Single"
		"melee_hit"			"Weapon_Crowbar.Melee_Hit"
		"melee_hit_world"	"Weapon_Crowbar.Melee_HitWorld"
	}


	TextureData
	{
		"weapon"
		{
				"font"		"SMODWeaponIcons"
				"character"	"k"
		}
		"weapon_s"
		{	
				"font"		"SMODWeaponIconsSelected"
				"character"	"k"
		}
		"weapon_small"
		{	
				"font"		"SMODWeaponIconsSmall"
				"character"	"k"
		}
		"ammo"
		{
				"font"		"SMODWeaponIconsSelected"
				"character"	"k"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"			"0"
			"y"			"48"
			"width"		"24"
			"height"	"24"
		}
	}
}