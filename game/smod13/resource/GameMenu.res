"GameMenu"
{
	"1"
	{
		"label" "#GameUI_GameMenu_ResumeGame"
		"command" "ResumeGame"
		"OnlyInGame" "1"
	}
	"2"
	{
		"label" "#GameUI_GameMenu_Disconnect"
		"command" "Disconnect"
		"OnlyInGame" "1"
	}
//	"3"
//	{
//		"label" "#GameUI_GameMenu_PlayerList"
//		"command" "OpenPlayerListDialog"
//		"OnlyInGame" "1"
//		"notsingle" "1"
//	}
	"4"	
	{
		"label" "#GameUI_GameMenu_NewGame"
		"command" "OpenNewGameDialog"
		"notmulti" "1"
	}
	"5"
	{
		"label" "#GameUI_GameMenu_LoadGame"
		"command" "OpenLoadGameDialog"
		"notmulti" "1"
	}
	"6"
	{
		"label" "#GameUI_GameMenu_SaveGame"
		"command" "OpenSaveGameDialog"
		"notmulti" "1"
		"OnlyInGame" "1"
	}
	"7"
	{
		"label" ""
		"command" ""
		"notsingle" "1"
	}
//	"8"
//	{
//		"label" "#GameUI_GameMenu_FindServers"
//		"command" "OpenServerBrowser"
//		"notmulti" "1"
//		"notsingle" "1"
//	}
//	"9"
//	{
//		"label" "#GameUI_GameMenu_CreateServer"
//		"command" "OpenCreateMultiplayerGameDialog"
//		"notsingle" "1"
//		"notmulti" "1"
//	}
//	"10"
//	{
//		"label" ""
//		"command" ""
//		"notsingle" "1"
//	}
	"11"
	{
		"label" "#GameUI_GameMenu_ActivateVR"
		"command" "engine vr_activate"
		"OnlyWhenVREnabled" "1"
		"OnlyWhenVRInactive" "1"
	}
	"12"
	{
		"label" "#GameUI_GameMenu_DeactivateVR"
		"command" "engine vr_deactivate"
		"OnlyWhenVREnabled" "1"
		"OnlyWhenVRActive" "1"
	}
	"13"
	{
		"label" "#GameUI_Controller"
		"command" "OpenControllerDialog"
		"ConsoleOnly" "1"
	}
	"14"
	{
		"label" "#GameUI_GameMenu_Options"
		"command" "OpenOptionsDialog"
	}
	"15"
	{
		"label" "#GameUI_GameMenu_Quit"
		"command" "Quit"
	}
}

